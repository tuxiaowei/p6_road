package arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Vector;

/*
 * @author 13177
 * @date 2021/12/30
 */
public class TestArrayList {

    public static void main(String[] args) {
        ArrayList<Integer> objects = new ArrayList<>();
        objects.add(1);

        objects.get(0);

        objects.set(0,2);

        objects.add(1,2);

        Vector<Object> objects1 = new Vector<>();
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.add(1);
        objects1.get(1);
    }

    private void testCopy(){
        Integer[] src = new Integer[]{1,2,999};
        Integer[] dest = new Integer[5];
        System.arraycopy(src, 0, dest, 0, 3);


        Integer integer = src[2];
        Integer integer1 = dest[2];
        System.out.println(integer == integer1);
        dest[0] = 2;

        System.out.println(src[0]);
        System.out.println(dest[0]);
    }
}
