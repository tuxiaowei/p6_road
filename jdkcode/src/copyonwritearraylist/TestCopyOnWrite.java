package copyonwritearraylist;

import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * @author 13177
 * @date 2022/1/5
 */
public class TestCopyOnWrite {

    private Object[] array = new Object[0];

    public static void main(String[] args) {
        CopyOnWriteArrayList<Object> objects = new CopyOnWriteArrayList<>();
        objects.add(9);
        objects.add(10);
        objects.add(1,2);

        System.out.println(objects.get(0));
        System.out.println(objects.get(1));
        System.out.println(objects.get(2));

        System.out.println(objects.size());

//        TestCopyOnWrite testCopyOnWrite = new TestCopyOnWrite();
//        Object[] array = testCopyOnWrite.getArray();
//        Object[] objects1 = Arrays.copyOf(array, array.length + 1);
//        for (Object o : objects1) {
//            System.out.println(o);
//        }
    }


    public Object[] getArray(){
        return array;
    }

}
